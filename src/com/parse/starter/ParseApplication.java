package com.parse.starter;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.app.Application;
import android.widget.Toast;

public class ParseApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// Add your initialization code here
		Parse.initialize(this, "2WWsscxOakZzj5oWDW5Ig1y0zqs5AQY1RcgTylQo", "1AD1o9T5uBX2hIHciRcHH3bcpc28zBx2fARrV5OF");


		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
	    
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		
		ParseACL.setDefaultACL(defaultACL, true);
		
		ParseUser currentUser = ParseUser.getCurrentUser();
		
		if(currentUser == null){
			ParseUser user = new ParseUser();
			user.setUsername("aekta");
			user.setPassword("aekta");
			user.setEmail("aekta@blackid.com");
			  
			// other fields can be set just like with ParseObject
			user.put("phone", "2500-85-9685");
			  
			user.signUpInBackground(new SignUpCallback() {
			  public void done(ParseException e) {
			    if (e == null) {
			      // Hooray! Let them use the app now.
			    	Toast.makeText(ParseApplication.this, "User Created Successfully", Toast.LENGTH_LONG).show();
			    } else {
			    	Toast.makeText(ParseApplication.this, "There is error in user creation", Toast.LENGTH_LONG).show();
			      // Sign up didn't succeed. Look at the ParseException
			      // to figure out what went wrong
			    }
			  }
			});
		}
		
		
	}

}
