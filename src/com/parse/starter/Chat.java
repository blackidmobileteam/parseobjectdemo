package com.parse.starter;

public class Chat {
	String userName;
	String message;
	
	public Chat() {
		// TODO Auto-generated constructor stub
	}
	
	public Chat(String userName,String message){
		this.userName 	= userName;
		this.message	= message;
	}
	
	public void setUsername(String name){
		userName = name;
	}
	
	public void setMessage(String msg){
		message = msg;
	}
	
	public String getUsername(){
		return userName;
	}
	
	public String getMessage(){
		return message;
	}
}
