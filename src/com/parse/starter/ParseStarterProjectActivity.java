package com.parse.starter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.adapter.ChatListAdapter;

public class ParseStarterProjectActivity extends Activity {
	
	List<ParseObject> chatServerObjects;
	Button send;
	EditText messageText;
	String message;
	ListView chatList;
	ChatListAdapter listAdapter;
	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		send		= (Button)findViewById(R.id.send);
		messageText	= (EditText)findViewById(R.id.message);
		chatList	= (ListView)findViewById(R.id.chat_list);
		
		ParseAnalytics.trackAppOpened(getIntent());
		final ParseUser user = ParseUser.getCurrentUser();
		
		chatServerObjects = new ArrayList<ParseObject>();
		
		fetchObjects();
		
		send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				message = messageText.getText().toString();
				if(!message.equalsIgnoreCase("") && message != null){
					ParseObject chatObject = new ParseObject("Chat");
					chatObject.put("Username", user.getUsername());
					chatObject.put("Message", message);
					//chatObject.saveInBackground();
					try {
						chatObject.save();
						messageText.setText("");
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Toast.makeText(ParseStarterProjectActivity.this, "data inserted", Toast.LENGTH_LONG).show();
					fetchObjects();
				}
			}
		});
		
	}
	
	public void fetchObjects(){
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Chat");
		
		if(chatServerObjects.size() >0){
			chatServerObjects.clear();
		}
		
		try {
			chatServerObjects = query.find();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		listAdapter = new ChatListAdapter(chatServerObjects, ParseStarterProjectActivity.this);
		chatList.setAdapter(listAdapter);
	}
}
