package com.parse.adapter;

import java.util.List;

import com.parse.ParseObject;
import com.parse.starter.*;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ChatListAdapter extends BaseAdapter{
	
	List<ParseObject> chatObjects;
	Context context;
	LayoutInflater inflater;
	
	TextView userName;
	TextView userMessage;
	
	public ChatListAdapter(List<ParseObject> chatObj,Context con) {
		// TODO Auto-generated constructor stub
		chatObjects = chatObj;
		context		= con;
		inflater 	= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return chatObjects.size();
	}

	@Override
	public ParseObject getItem(int position) {
		// TODO Auto-generated method stub
		return chatObjects.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		convertView = inflater.inflate(R.layout.chat_list_item, parent, false);
		
		userName	= (TextView)convertView.findViewById(R.id.user_name);
		userMessage	= (TextView)convertView.findViewById(R.id.user_message);
		
		userName.setText(chatObjects.get(position).getString("Username"));
		userMessage.setText(chatObjects.get(position).getString("Message"));
		
		return convertView;
	}

}
